import sys
from os.path import join, abspath, dirname
import pytest
from . import context
from madx import parser


def test_parse_lhc(_lhc_lattice_path):
    with open(_lhc_lattice_path, "r") as lhc_lattice:
        parser.madx_parser.parse(lhc_lattice.read())


def test_parse_alba(_alba_lattice_path):
    with open(_alba_lattice_path, "r") as alba_lattice:
        parser.madx_parser.parse(alba_lattice.read())


def test_parse_command():
    text = "beam, sequence=Machine, particle=electron, energy=3;"
    parser.madx_parser.parse(text)


def test_parse_definition():
    text = "QD2: QUADRUPOLE, L=0.29, K1S=-1.877886175469, TILT=pi/4.;"
    parser.madx_parser.parse(text)


def test_parse_assigment():
    text = "var := ((3 * sin(4 / 3)^4) + 7) * 3e-5;"
    parser.madx_parser.parse(text)


def test_parse_line():
    text = "BLOCK1: LINE=(L_ID,QD1,D11,QF1,D12,SF1,D13,QF2,D14,SD1,D15);"
    parser.madx_parser.parse(text)


def test_parse_if():
    parser.madx_parser.parse("""
    IF (a > 34.5) { value, 342; }
    ELSEIF ( b->meh <> (34 + 7)) { qmf: quadrupole, l=1; }
    ELSEIF ( b->meh == (34 + 7)) { qmf: quadrupole, l=4; }
    ELSE { title "something"; }
    """)


def test_parse_while():
    parser.madx_parser.parse("""
    WHILE (45 > 32 && (abc <> 78) || (4 == (abc*3))) { value, meh; };
    """)


def test_declare_macro():
    parser.madx_parser.parse("""
    some_macro: MACRO = { beam, energy=750*x*y*z; };
    """)


def test_declare_macro_params():
    parser.madx_parser.parse("""
    some_macro(x, y,z): MACRO = { beam, energy=750*x*y*z; };
    """)


def test_exec_macro():
    parser.madx_parser.parse("""
    exec, some_macro(x, y, z);
    """)


def test_ignores_comments():
    parser.madx_parser.parse("""
    // a comment
    beam, sequence=Machine;
    /*

    block comment

         */
         ! Ignore this!
    """)


@pytest.fixture
def _alba_lattice_path():
    return abspath(join(dirname(__file__), "..",
                        "sequences", "ALBA-25.4th_v2.seq"))


@pytest.fixture
def _lhc_lattice_path():
    return abspath(join(dirname(__file__), "..",
                        "sequences", "lhc_as-built.seq"))
