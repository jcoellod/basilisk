from typing import Any, List
from madx import bsk_interface as itf


class OperableMixin(object):
    def __add__(self, value): return self.to_expr().__add__(value)
    def __sub__(self, value): return self.to_expr().__sub__(value)
    def __mul__(self, value): return self.to_expr().__mul__(value)
    def __div__(self, value): return self.to_expr().__div__(value)
    def __truediv__(self, value): return self.to_expr().__truediv__(value)
    def __pow__(self, value): return self.to_expr().__pow__(value)
    def __radd__(self, value): return self.to_expr().__radd__(value)
    def __rsub__(self, value): return self.to_expr().__rsub__(value)
    def __rmul__(self, value): return self.to_expr().__rmul__(value)
    def __rdiv__(self, value): return self.to_expr().__rdiv__(value)
    def __rtruediv__(self, value): return self.to_expr().__rtruediv__(value)
    def __rpow__(self, value): return self.to_expr().__rpow__(value)
    def to_expr(self):
        # type: () -> Expression
        raise NotImplementedError("Operables must implement to_expr.")


class Variable(OperableMixin):

    PREFIX = "mdx_"

    def __init__(self, value=None, name=None, dynamic=False):
        # type: (float, str, bool) -> None
        if name is not None:
            self._raw_var = itf.get(name)
            self._madx_name = name
        elif name is None and value is not None:
            self._madx_name = self._unique_name()
            self.set(value, dynamic=dynamic)
        else:
            raise ValueError("One of name or value must be given.")

    def set(self, value, dynamic=False):
        # type: (Any[float, OperableMixin], bool) -> None
        value = _wrap_on_expr(value)
        if dynamic:
            itf.set_dynamic(self._madx_name, value)
        else:
            itf.set_static(self._madx_name, float(value))

    def to_expr(self):
        # type: () -> Expression
        expr = Expression()
        expr.append(self._madx_name)
        return expr

    def _unique_name(self):
        # type: () -> str
        return Variable.PREFIX + str(id(self))

    def __float__(self):
        # type: () -> float
        return float(self.to_expr())


class Expression(list, OperableMixin):

    @staticmethod
    def cast(str_list):
        # type: (List[str]) -> Expression
        newexpr = Expression()
        newexpr.extend(str_list)
        return newexpr

    @staticmethod
    def _operate(left, op, right):
        # type: (Expression, str, Any[Expression, Variable]) -> Expression
        expr_left, expr_right = (_wrap_on_expr(left), _wrap_on_expr(right))
        newexpr = (["("] + list(expr_left) + [")"] +
                   [op] +
                   ["("] + list(expr_right) + [")"])
        return Expression.cast(newexpr)

    def __add__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "+", expression)

    def __sub__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "-", expression)

    def __mul__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "*", expression)

    def __div__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "/", expression)

    def __truediv__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "/", expression)

    def __pow__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(self, "^", expression)

    def __radd__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "+", self)

    def __rsub__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "-", self)

    def __rmul__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "*", self)

    def __rdiv__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "/", self)

    def __rtruediv__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "/", self)

    def __rpow__(self, expression):
        # type: (Any[Expression, Variable, float]) -> Expression
        return Expression._operate(expression, "^", self)

    def __float__(self):
        # type: () -> float
        return float(itf.expression(self))

    def __str__(self):
        # type: () -> str
        return "".join(self)

    def to_expr(self):
        # type: () -> Expression
        return self


def _wrap_on_expr(value):
    # type: (Any) -> Expression
    try:
        return value.to_expr()
    except (NotImplementedError, AttributeError):
        return Expression.cast([str(value)])
