from __future__ import absolute_import
from os.path import join, dirname, abspath, pardir
from typing import Iterable, List
import ctypes
import numpy as np
from madx import structs as mst

ENCODING = "utf-8"

# MAD-X value that indicates wrong expression
INVALID = 1e20

LIB_PATH = abspath(join(dirname(__file__), pardir, pardir, "lib"))

_LIB = np.ctypeslib.load_library("libmadx64", LIB_PATH)


_LIB.madx_start()
_VARIABLE_LIST = ctypes.POINTER(mst.VarList)\
                       .in_dll(_LIB, "variable_list")
_ELEMENT_LIST = ctypes.POINTER(mst.VarList)\
                      .in_dll(_LIB, "element_list")
_IN = ctypes.POINTER(mst.InBuffList).in_dll(_LIB, "in")


_pro_input = _LIB.pro_input_
_pro_input.argtypes = (ctypes.c_char_p, )


_memopen = _LIB.fmemopen
_memopen.argtypes = (ctypes.c_char_p,
                     ctypes.c_size_t,
                     ctypes.c_char_p)
_memopen.restype = ctypes.c_void_p


_get_stmt = _LIB.get_stmt
_get_stmt.argtypes = (ctypes.c_void_p, ctypes.c_int)
_get_stmt.restype = ctypes.c_int


_stolower_nq = _LIB.stolower_nq
_stolower_nq.argtypes = (ctypes.c_char_p, )


def raw_input(raw_str):
    # type: (str) -> None
    char_array = ctypes.c_char_p(raw_str.encode(ENCODING))
    memfile_stream = _memopen(
        char_array,
        ctypes.c_size_t(len(raw_str) * ctypes.sizeof(ctypes.c_char)),
        ctypes.c_char_p("r".encode(ENCODING))
    )
    while True:
        res = _get_stmt(memfile_stream, 0)
        if not res:
            break
        this_in = _IN.contents
        curren_stmt = this_in.buffers[this_in.curr].contents.c_a.contents.c
        _stolower_nq(curren_stmt)
        _pro_input(curren_stmt)


_find_variable = _LIB.find_variable
_find_variable.argtypes = (ctypes.c_char_p,
                           ctypes.POINTER(mst.VarList))
_find_variable.restype = ctypes.POINTER(mst.Variable)


def get(varname):
    # type: (str) -> mst.Variable
    var = _find_variable(varname.encode(ENCODING), _VARIABLE_LIST)
    if not var:
        raise ValueError("No variable with name {} defined.".format(varname))
    return var


def set(var):
    # type: (expr.Variable) -> None
    madx_var = mst.Variable()
    madx_var.name = var._madx_name


_double_from_expr = _LIB.double_from_expr
_double_from_expr.argtypes = (ctypes.POINTER(ctypes.c_char_p),
                              ctypes.c_int,
                              ctypes.c_int)
_double_from_expr.restype = ctypes.c_double


_make_expression = _LIB.make_expression
_make_expression.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))
_make_expression.restype = ctypes.POINTER(mst.Expression)


def _token_array_to_pointer(expr_tokens):
    # type: (List[str]) -> ctypes.c_char_p
    tokens_arr = (ctypes.c_char_p * len(expr_tokens))()
    tokens_arr[:] = [token.encode(ENCODING) for token in expr_tokens]
    return ctypes.POINTER(ctypes.c_char_p)(tokens_arr)


def expression(tokens):
    # type: (List[str]) -> Option[float]
    tokens_arr = _token_array_to_pointer(tokens)
    value = _double_from_expr(tokens_arr, 0, len(tokens) - 1)
    if value == INVALID:
        raise ValueError("Invalid expression: {}".format("".join(tokens)))
    return value


_find_sequence = _LIB.find_sequence
_find_sequence.argtypes = (ctypes.c_char_p, ctypes.c_void_p)
_find_sequence.restype = ctypes.POINTER(mst.Sequence)


def find_sequence(name):
    # type: (List[str]) -> mst.Sequence
    seq = _find_sequence(name.encode(ENCODING), ctypes.c_void_p())
    if not seq:
        raise ValueError("No sequence with name {} defined.".format(name))
    return seq.contents


_find_table = _LIB.find_table
_LIB.find_table.argtypes = (ctypes.c_char_p, )
_LIB.find_table.restype = ctypes.POINTER(mst.Table)


_make_table = _LIB.make_table
_make_table.argtypes = (ctypes.c_char_p, ctypes.c_char_p,
                        ctypes.POINTER(ctypes.c_char_p),
                        ctypes.POINTER(ctypes.c_int),)
_make_table.restype = ctypes.POINTER(mst.Table)


_table_register = ctypes.POINTER(mst.TableList)\
                        .in_dll(_LIB, "table_register")
_add_to_table_list = _LIB.add_to_table_list
_add_to_table_list.argtypes = (ctypes.POINTER(mst.Table),
                               ctypes.POINTER(mst.TableList))
_add_to_table_list.restype = None


def find_table(name):
    # type: (List[str]) -> mst.Table
    table = _find_table(name.encode(ENCODING), ctypes.c_void_p())
    if not table:
        raise ValueError("No table with name {} defined.".format(name))
    return table.contents


_new_variable = _LIB.new_variable
_new_variable.argtypes = (ctypes.c_char_p, ctypes.c_double,
                          ctypes.c_int, ctypes.c_int,
                          ctypes.POINTER(mst.Expression),
                          ctypes.c_char_p)
_new_variable.restype = ctypes.POINTER(mst.Variable)


_add_to_var_list = _LIB.add_to_var_list
_add_to_var_list.argtypes = (ctypes.POINTER(mst.Variable),
                             ctypes.POINTER(mst.VarList),
                             ctypes.c_int)
_add_to_var_list.restype = None


def set_str(name, str_val):
    # type: (str, str) -> mst.Variable
    var = _LIB.new_variable(name.encode(ENCODING),
                            0, 3, 0, None,
                            str_val.encode(ENCODING))
    _add_to_var_list(var, _VARIABLE_LIST, ctypes.c_int(0))
    return var


def set_static(name, value):
    # type: (str, float) -> mst.Variable
    var = _LIB.new_variable(name.encode(ENCODING),
                            value, 1, 1, None, None)
    _add_to_var_list(var, _VARIABLE_LIST, ctypes.c_int(0))
    return var


def set_dynamic(name, expr_val):
    # type: (str, expr.Expression) -> mst.Variable
    raw_expr_p = _make_expression(
        len(expr_val),
        _token_array_to_pointer(expr_val)
    )
    var = _LIB.new_variable(name.encode(ENCODING), 0, 1, 2, raw_expr_p, None)
    _add_to_var_list(var, _VARIABLE_LIST, ctypes.c_int(0))
    return var


_find_element = _LIB.find_element
_find_element.argtypes = (ctypes.c_char_p,
                          ctypes.POINTER(mst.ElList))
_find_element.restype = ctypes.POINTER(mst.Element)


def find_element(name):
    # type: (str) -> mst.Element
    elem = _find_element(name.encode(ENCODING), _ELEMENT_LIST)
    if not elem:
        raise ValueError("No element with name {} defined.".format(name))
    return elem.contents
