from typing import Any, Callable
from expressions.expression import Variable, Expression
from expressions.table import Table
from expressions.sequence import Sequence, Element
from madx import bsk_interface as itf


def raw(command):
    # type: (str) -> None
    """Sends command directly to the MAD-X system
    Arguments:
        command: String of MAD-X code, with no comments (for now).
    TODO: It does not accept comments.
    """
    itf.raw_input(command)


def call(path):
    # type: (str) -> None
    """Launch a MAD-X call command to run an external MAD-X file.
    Arguments:
        path: The path to the file to run.
    Raises:
        IOError: If the file does not exist.
    """
    open(path, "r").close()  # Test that the file exists
    itf.raw_input("call, file = {};".format(path))


def control(name):
    # type: (str) -> Variable
    """Returns a variable that handles an existing variable in MAD-X.

    Arguments:
        name: Name of the variable to control.
    Raises:
        ValueError: If the variable is not defined in MAD-X.
    """
    return Variable(name=name)


def static(value):
    # type: (Any[float, Variable, Expression]) -> Variable
    """Creates and return a new variable in MAD-X assigned to a float value.

    This variable will hold a simple value, evaluated on definition, equivalent
    to the to the MAD-X = assignment.
    Arguments:
        value: The value to assign to the new variable in MAD-X.
    """
    return Variable(value=value, dynamic=False)


def dynamic(value):
    # type: (Any[float, Variable, Expression]) -> Variable
    """Creates and return a new variable in MAD-X with delayed evaluation.

    The value of this variable will be evaluated on the fly, equivalent
    to the MAD-X := assignment.
    Arguments:
        value: The value to assign to the new variable in MAD-X.
    """
    return Variable(value=value, dynamic=True)


def table(name):
    # type: (str) -> Table
    """Returns a Python representation of a MAD-X table.

    Arguments:
        name: The name of the table to wrap.
    Raises:
        ValueError: If the requested table does not exist.
    """
    return Table(name)


def sequence(name):
    # type: (str) -> Sequence
    """Returns a Python representation of a MAD-X sequence.

    Arguments:
        name: The name of the sequence to wrap.
    Raises:
        ValueError: If the requested sequence does not exist.
    """
    return Sequence.find_sequence(name)


def element(name):
    # type: (str) -> Element
    """Returns a Python representation of a MAD-X element.

    Arguments:
        name: The name of the element to wrap.
    Raises:
        ValueError: If the requested element does not exist.
    """
    return Element.find_element(name)


def madx(funct):
    # type: (Callable) -> Callable
    """Decorator to apply to a function with MAD-X code as docstring.

    This decorator will first execute the docstring of the passed function as
    MAD-X code and then run the function itself. It will format the MAD-X code
    with the passed keyword arguments. It will raise ValueError if possitional
    arguments are given.
    """
    def wrapper(*args, **kwargs):
        if args:
            raise ValueError(
                "Pure MAD-X functions do not accept positional arguments."
            )
        if funct.__doc__ is not None:
            raw(funct.__doc__.format(**kwargs))
        return funct(*args, **kwargs)
    return wrapper
