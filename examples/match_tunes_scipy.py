from __future__ import print_function
import sys
from os.path import join
import numpy as np
from scipy.optimize import root
import context
import basilisk as bsk

AFS_ROOT = r"\\AFS" if sys.platform.startswith("win") else r"/afs"
OPTICS_2018 = join(AFS_ROOT, "cern.ch", "eng", "lhc",
                   "optics", "runII", "2018")
MADX_MODE = False


def do_match_tunes():
    bsk.call(join(OPTICS_2018, "lhc_as-built.seq"))
    bsk.call(join(OPTICS_2018, "PROTON", "opticsfile.22_ctpps2"))
    bsk.raw("beam,particle=proton,energy=6500,bv=+1,sequence=lhcb1;")
    lhcb1 = bsk.sequence("lhcb1")
    lhcb1.use()
    if MADX_MODE:
        bsk.raw("""
        match;
        vary, name=dQx.b1;
        vary, name=dQy.b1;
        constraint, range=#E, mux=62.28, muy=60.31;
        lmdif, calls=1000, tolerance=1e-10;
        endmatch;
        """)
    else:
        initial = np.array([0, 0])
        targets = np.array([62.28, 60.31])
        res = root(lambda x: adapt_tunes(x, targets),
                   initial,
                   tol=1e-3, method='hybr',
                   options={"eps": 1e-4, "xtol": 1e-3})
        print(res)


def adapt_tunes(input_, targets):
    dqx, dqy = input_
    bsk.raw("dqx.b1={0}; dqy.b1={1};".format(dqx, dqy))
    bsk.raw("twiss, notable;")
    bsk.raw("nqx := table(summ, q1); nqy := table(summ, q2);")
    nqx = float(bsk.control("nqx"))
    nqy = float(bsk.control("nqy"))
    targetx, targety = targets
    lossx = np.abs(targetx - nqx)
    lossy = np.abs(targety - nqy)
    print("Current results: ", nqx, nqy)
    print("Current loss: ", lossx, lossy)
    return lossx, lossy


if __name__ == "__main__":
    do_match_tunes()
