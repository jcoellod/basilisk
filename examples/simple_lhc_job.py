from __future__ import print_function
import sys
from os.path import join
import context
import basilisk as bsk

AFS_ROOT = r"\\AFS" if sys.platform.startswith("win") else r"/afs"
OPTICS_2018 = join(AFS_ROOT, "cern.ch", "eng", "lhc",
                   "optics", "runII", "2018")


def do_lhc_twiss():
    bsk.call(join(OPTICS_2018, "lhc_as-built.seq"))
    bsk.call(join(OPTICS_2018, "PROTON", "opticsfile.22_ctpps2"))
    bsk.raw("beam, particle=proton, energy=6500, bv=+1, sequence=lhcb1;")
    lhcb1 = bsk.sequence("lhcb1")
    lhcb1.use()
    bsk.raw("twiss;")
    twisstable = bsk.table("twiss")
    print("beta-function at bpm.12r1.b1: {:.2f} m"
          .format(float(twisstable["bpm.12r1.b1", "bety"])))



if __name__ == "__main__":
    do_lhc_twiss()
