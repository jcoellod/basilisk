import ctypes
from collections import OrderedDict
import pandas as pd
import numpy as np
from madx import bsk_interface as itf
from madx import structs
from expressions.expression import Expression, OperableMixin


class Table(object):

    def __init__(self, name):
        # type: (str) -> None
        self._table = itf.find_table(name)
        self.name = name

    def __getitem__(self, rowcol):
        # type: (str) -> TableElem
        rowname, colname = rowcol
        return Table.TableElem(self, rowname, colname)

    @staticmethod
    def from_dataframe(tabname, tabtype, table_df):
        numrows, numcols = table_df.shape
        colnames = (ctypes.c_char_p * (numcols + 1))()
        # The empty string marks the end of the array.
        colnames[:] = [str(x) for x in table_df.columns] + [" "]
        colnames = ctypes.POINTER(ctypes.c_char_p)(colnames)
        coltypes = (ctypes.c_int * numcols)()
        coltypes[:] = [3 if table_df.iloc[:, x].dtype == np.object else 2
                       for x in range(numcols)]
        coltypes = ctypes.POINTER(ctypes.c_int)(coltypes)
        newtab = itf._make_table(
            tabname, tabtype, colnames, coltypes, numrows,
        )
        if not newtab:
            raise ValueError("Could not create table {}.".format(tabname))
        itf._add_to_table_list(newtab, itf._table_register)
        newtab = newtab.contents
        newtab.curr = numrows
        for index, colname in enumerate(table_df):
            series = table_df.loc[:, colname]
            if series.dtype == np.object:
                data = (ctypes.c_char_p * numrows)()
                data[:] = [str(x) for x in series.values]
                data = ctypes.cast(data, ctypes.POINTER(ctypes.c_char_p))
                newtab.s_cols[index] = data
                continue
            pointer = series.values.ctypes\
                                   .data_as(ctypes.POINTER(ctypes.c_double))
            newtab.d_cols[index] = pointer
        return Table(tabname)

    def as_dataframe(self):
        table = self._table
        nametypes = table.name_list.contents
        s_cols_iter = table.s_cols
        d_cols_iter = table.d_cols
        df_dict = OrderedDict()
        for index in range(nametypes.curr):
            colname = nametypes.names[index]
            colname = colname.upper()
            if nametypes.inform[index] == 3:  # string
                df_dict[colname] = Table._expand_string_col(s_cols_iter[index],
                                                            table.curr)
            else:
                df_dict[colname] = Table._expand_double_col(d_cols_iter[index],
                                                            table.curr)
        hdict = self._headers_from_table()
        df_res = pd.DataFrame(data=df_dict)
        return df_res, hdict

    def _headers_from_table(self):
        hdict = OrderedDict([("NAME", self._table.name),
                             ("TYPE", self._table.type)])
        headers = self._table.header
        if headers:
            headers = self._table.header.contents
            for index in range(headers.curr):
                raw_header = headers.p[index]
                _, hname, rawtype, value = raw_header.split()
                hdict[hname] = (float(value)
                                if rawtype == "%le"
                                else value.strip('"'))
        return hdict

    @staticmethod
    def _expand_string_col(c_string_p, length):
        return np.array([c_string_p[index] for index in range(length)])

    @staticmethod
    def _expand_double_col(c_double_p, length):
        buff = np.core.multiarray.int_asbuffer(
            ctypes.addressof(c_double_p.contents), 8*length
        )
        return np.frombuffer(buff, np.double)

    class TableElem(OperableMixin):
        def __init__(self, table, rowname, colname):
            # type: (Table, str, str) -> None
            self._table = table
            self._rowname = rowname
            self._colname = colname
            self._fake_varname = "__{}__".format(self._table.name)
            itf.set_str(
                self._fake_varname,
                " ".join((self._table.name, rowname, colname))
            )

        def to_expr(self):
            # type: () -> Expression
            expr = Expression()
            expr.extend(("table", "(", self._fake_varname, ")"))
            return expr

        def __float__(self):
            return float(itf.expression(self.to_expr()))
