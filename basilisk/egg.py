"""Convenience module to launch basilisk tasks in subprocesses.

As importing the basilisk module will cause the MAD-X system to pollute the
process memory, this module contains a set of functions to easily launch and
retrieve results from MAD-X. This also allow to launch several basilisk
processes in parallel that would otherwise share MAD-X memory.
"""
import multiprocessing as mp


def spawn_basilisk(function, *args, **kwargs):
    """Spawns a new process calling a function in it with basilisk.

    This function will create a new process and call the function passed with
    the basilisk module as first argument.
    """
    pool = mp.Pool(processes=1)
    ret_obj = pool.apply(_inject_basilisk, [function] + list(args), kwargs)
    pool.close()
    pool.join()
    return ret_obj


def spawn_nest(function, list_of_args, processes=mp.cpu_count()):
    """Spawns one process per element in the list_of args.

    Same as spawn_basilisk but this one will launch one process per element on
    the list_of_arguments iterable, basically a convenience wrapper on the
    multiprocessing Pool map function. The functions must only accept
    positional arguments.
    """
    pool = mp.Pool(processes=processes)
    ret_obj = pool.map(
        _unpacker_for_pool,
        [(function, args) for args in list_of_args],
    )
    pool.close()
    pool.join()
    return ret_obj


def spawn_this(function):
    """Decorator acting like spawn_basilisk on the decorated function.

    When the decorated function is called it will be launched in a separate
    process with the basilisk module as first argument.
    """
    def wrapper(*args, **kwargs):
        return spawn_basilisk(function, *args, **kwargs)
    return wrapper


def _inject_basilisk(function, *args, **kwargs):
    import basilisk
    return function(basilisk, *args, **kwargs)


def _unpacker_for_pool(function_and_args):
    function, args = function_and_args
    return _inject_basilisk(function, *list(args))
