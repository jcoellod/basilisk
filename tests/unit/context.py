import sys
from os.path import abspath, join, dirname, pardir

root_path = abspath(join(dirname(__file__), pardir, pardir))
bsk_path = join(root_path, "basilisk")

if bsk_path not in sys.path:
    sys.path.insert(0, bsk_path)
