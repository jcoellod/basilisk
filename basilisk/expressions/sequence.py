from contextlib import contextmanager
from typing import Any, Generator, Optional
from madx import bsk_interface as itf
from madx import structs as sts


class Sequence(object):

    def __init__(self, raw_seq):
        # type: (sts.Sequence) -> None
        self._raw_seq = raw_seq
        self.name = raw_seq.name

    @staticmethod
    def find_sequence(name):
        raw_seq = itf.find_sequence(name)
        if raw_seq is None:
            raise ValueError("Unknown sequence {}.".format(name))
        return Sequence(raw_seq)

    def use(self):
        # type: () -> None
        itf.raw_input("use, sequence = {};".format(self.name))

    def sequence_nodes(self):
        # type: () -> Generator[Node, None, None]
        node = Node(self._raw_seq.start.contents)
        while True:
            if not node:
                raise StopIteration
            yield node
            node = node.next_node()

    @contextmanager
    def edit(self):
        # type: () -> Generator[SeqEditor, None, None]
        itf.raw_input("seqedit, sequence={};".format(self.name))
        yield SeqEditor(self)
        itf.raw_input("endedit;")

    def __getitem__(self, name):
        # type: (str) -> Node
        pass


class SeqEditor(object):

    def __init__(self, sequence):
        # type: (Sequence) -> None
        pass


class Element(object):

    def __init__(self, raw_elem):
        self._raw_elem = raw_elem
        self.name = raw_elem.name
        self.length = raw_elem.length

    @staticmethod
    def find_element(name):
        return Element(itf.find_element(name))

    def params(self):
        command = getattr(self._raw_elem, "def")
        param_list = command.contents.par.contents
        for index in range(param_list.curr):
            yield param_list.parameters[index].contents

    @property
    def parent(self):
        # type: () -> Element
        parent = self._raw_elem.parent.contents
        if not parent:
            raise ValueError("Element {} has not parent.".format(self.name))
        return Element(parent)


class Node(object):

    def __init__(self, raw_node):
        # type: (sts.Node) -> None
        self._raw_node = raw_node
        self.name = self._raw_node.name
        self.position = self._raw_node.position

    @property
    def element(self):
        # type: () -> Optional[Element]
        element = self._raw_node.element.contents
        if not element:
            raise ValueError(
                "Node {} has not associated element".format(self.name)
            )
        return Element(element)

    def next_node(self):
        # type: () -> Node
        return Node(self._raw_node.next.contents)

    def previous_node(self):
        # type: () -> Node
        return Node(self._raw_node.previous.contents)
