import ctypes


_NAME_L = 48  # Name length in MAD-X

# Forward definitions:

class IntArray(ctypes.Structure): pass
class CharPArray(ctypes.Structure): pass
class CharArray(ctypes.Structure): pass
class NameList(ctypes.Structure): pass
class Table(ctypes.Structure): pass
class TableList(ctypes.Structure): pass
class Variable(ctypes.Structure): pass
class VarList(ctypes.Structure): pass
class Element(ctypes.Structure): pass
class ElList(ctypes.Structure): pass
class DoubleArray(ctypes.Structure): pass
class Sequence(ctypes.Structure): pass
class Node(ctypes.Structure): pass
class NodeList(ctypes.Structure): pass
class Command(ctypes.Structure): pass
class CommandParameter(ctypes.Structure): pass
class CommandParameterList(ctypes.Structure): pass
class Expression(ctypes.Structure): pass
class ExprList(ctypes.Structure): pass
class InBuffList(ctypes.Structure): pass
class InBuff(ctypes.Structure): pass



# Fields definitions:

IntArray._fields_ = [("stamp", ctypes.c_int),
                     ("name", ctypes.c_char * _NAME_L),
                     ("max", ctypes.c_int),
                     ("curr", ctypes.c_int),
                     ("i", ctypes.POINTER(ctypes.c_int))]


CharPArray._fields_ = [("name", ctypes.c_char * _NAME_L),
                       ("max", ctypes.c_int),
                       ("curr", ctypes.c_int),
                       ("flag", ctypes.c_int),
                       ("stamp", ctypes.c_int),
                       ("p", ctypes.POINTER(ctypes.c_char_p))]


CharArray._fields_ = [("stamp", ctypes.c_int),
                      ("max", ctypes.c_int),
                      ("curr", ctypes.c_int),
                      ("c", ctypes.c_char_p)]


NameList._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),
    ("curr", ctypes.c_int),
    ("index", ctypes.POINTER(ctypes.c_int)),
    ("inform", ctypes.POINTER(ctypes.c_int)),
    ("stamp", ctypes.c_int),
    ("names", (ctypes.POINTER(ctypes.c_char_p))),
]


Table._fields_ = [
    ("name", ctypes.c_char * _NAME_L),  # like "twiss", "survey" etc.
    ("type", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. # rows
    ("curr", ctypes.c_int),  # current # rows
    ("num_cols", ctypes.c_int),  # total # columns - fixed
    ("org_cols", ctypes.c_int),  # original # columns from definition
    ("dynamic", ctypes.c_int),  # if != 0, values taken from current row
    ("origin", ctypes.c_int),  # 0 if created in job, 1 if read
    ("header", ctypes.POINTER(CharPArray)),  # extra lines for file header
    ("col_out", ctypes.POINTER(IntArray)),  # column no.s to be written (in this order)
    ("row_out", ctypes.POINTER(IntArray)),  # flag for row: 1 write, 0 don't
    ("node_nm", ctypes.POINTER(CharPArray)),  # names of nodes at each row
    ("l_head", ctypes.POINTER(ctypes.POINTER(CharPArray))),  # extra lines to be put in front of a line
    ("p_nodes", ctypes.c_voidp),  # pointers to nodes at each row
    ("s_cols", ctypes.POINTER(ctypes.POINTER(ctypes.c_char_p))),  # string columns
    ("d_cols", ctypes.POINTER(ctypes.POINTER(ctypes.c_double))),  # double precision columns
    ("stamp", ctypes.c_int),
    ("name_list", ctypes.POINTER(NameList)),  # names + types (in inform): 1 double, 3 string
    ("sequence", ctypes.c_voidp),  # pointer to sequence it refers to
]



TableList._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),
    ("curr", ctypes.c_int),
    ("names", ctypes.POINTER(NameList)),
    ("tables", ctypes.POINTER(ctypes.POINTER(Table))),
    ("stamp", ctypes.c_int),
]


Variable._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("status", ctypes.c_int),  # 0 value not evaluated, 1 evaluated
    ("type", ctypes.c_int),  # 0 constant, 1 direct, 2 deferred, 3 string
    ("val_type", ctypes.c_int),  # 0 int 1 double (0..2)
    ("string", ctypes.c_char_p),  # pointer to string if 3
    ("expr", ctypes.POINTER(Expression)),  # pointer to defining expression (0..2)
    ("value", ctypes.c_double),  # (0..2)
    ("stamp", ctypes.c_int),
]


VarList._fields_ = [
    ("stamp", ctypes.c_int),
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("name_list", ctypes.POINTER(NameList)),  # index list of names
    ("variable", ctypes.POINTER(ctypes.POINTER(Variable))),  # variable pointer list
]


Element._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("def_type", ctypes.c_int),  # 0 if defined separately, 1 if inside sequence
    ("bv", ctypes.c_int),  # 0 false, 1 true (invert angle for sequence bv = -1)
    ("length", ctypes.c_double),
    ("def", ctypes.POINTER(Command)),  # pointer to defining command
    ("parent", ctypes.POINTER(Element)),  # pointer to parent of element this for base_type elements (rbend etc.)
    ("stamp", ctypes.c_int),
    ("base_type", ctypes.POINTER(Element)),  # pointer to base_type of element
]


ElList.fields_ = [
    ("stamp", ctypes.c_int),
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("name_list", ctypes.POINTER(NameList)),  # index list of names
    ("elem", ctypes.POINTER(ctypes.POINTER(Element))),  # element pointer list
]


DoubleArray._fields_ = [
    ("stamp", ctypes.c_int),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("a", ctypes.POINTER(ctypes.c_double)),
]


Sequence._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("export_name", ctypes.c_char * _NAME_L),
    ("refpos", ctypes.c_char_p),  # reference position for insertion
    ("next_sequ", ctypes.c_char_p),  # name of following sequence (only survey)
    ("ref_flag", ctypes.c_int),  # -1 for exit, 0 for centre, 1 for entry
    ("share", ctypes.c_int),  # 0 normal, 1 if shared
    ("nested", ctypes.c_int),  # 0 flat, 1 if nested
    ("con_cnt", ctypes.c_int),  # constraint counter
    ("stamp", ctypes.c_int),
    ("line", ctypes.c_int),  # set to 1 if origin is a line
    ("add_pass", ctypes.c_int),  # number of additional passes
    ("num_interp", ctypes.c_int),  # number of additonal rows in the twiss table
    ("length", ctypes.c_double),  # length as in declaration
    ("l_expr", ctypes.POINTER(Expression)),  # length expression as in declaration
    ("start", ctypes.POINTER(Node)),  # first node in sequence
    ("end", ctypes.POINTER(Node)),  # last node in sequence
    ("nodes", ctypes.POINTER(NodeList)),  # alphabetic list of nodes
    ("cavities", ctypes.POINTER(ElList)),  # alphabetic list of cavities
    ("crabcavities", ctypes.POINTER(ElList)),  # alphabetic list of crab cavities
    ("beam", ctypes.POINTER(Command)),  # pointer to beam attached
    # expanded sequence
    ("n_nodes", ctypes.c_int),  # number of nodes when expanded
    ("start_node", ctypes.c_int),  # first node of current range in all_nodes
    ("pass_count", ctypes.c_int),  # number of executed passes
    ("ex_start", ctypes.POINTER(Node)),  # first node in expanded sequence
    ("ex_end", ctypes.POINTER(Node)),  # last node in expanded sequence
    ("range_start", ctypes.POINTER(Node)),  # first node of current range in sequence
    ("range_end", ctypes.POINTER(Node)),  # last node of current range in sequence
    ("all_nodes", ctypes.POINTER(ctypes.POINTER(Node))),  # sequential list of all nodes
    ("ex_nodes", ctypes.POINTER(NodeList)),  #  alphabetic list of nodes (no drifts)
    ("tw_table", ctypes.POINTER(Table)),  # pointer to latest twiss table created
    ("tw_valid", ctypes.c_int),  # true if current tw_table is valid
    ("cl", ctypes.c_void_p),  # pointer to constraint list during match
    ("orbits", ctypes.c_void_p),  # pointer to list of stored orbits
]


Node._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("base_name", ctypes.c_char_p),  # basic type
    ("previous", ctypes.POINTER(Node)),
    ("next", ctypes.POINTER(Node)),
    ("master", ctypes.POINTER(Node)),  # ancestor for interpolated nodes
    ("occ_cnt", ctypes.c_int),  # element occurrence count at node
    ("obs_point", ctypes.c_int),  # observation point number (tracking)
    ("sel_err", ctypes.c_int),  # error select flag
    ("sel_sector", ctypes.c_int),  # sectormap select flag
    ("con_cnt", ctypes.c_int),  # constraint counter
    ("enable", ctypes.c_int),  # flag for correctors and monitors: 0 off, 1 on
    ("moved", ctypes.c_int),  # temporary flag during sequence editing
    ("stamp", ctypes.c_int),
    ("pass_flag", ctypes.c_double),  # set to 1 at first pass of use-triggered survey
    ("position", ctypes.c_double),  # s position in sequence [m]
    ("at_value", ctypes.c_double),
    ("length", ctypes.c_double),
    ("dipole_bv", ctypes.c_double),  # +1 or -1 (if beam_bv AND element_bv)
    ("other_bv", ctypes.c_double),  # equal to beam_bv (+1 or -1)
    ("chkick", ctypes.c_double),  # calculated by orbit correction module
    ("cvkick", ctypes.c_double),  # calculated by orbit correction module
    ("surv_data", ctypes.c_double * 7),  # x,y,z,theta,phi,psi,s for use with survey
    ("at_expr", ctypes.POINTER(Expression)),
    ("from_name", ctypes.c_char_p),
    ("p_elem", ctypes.POINTER(Element)),  # pointer to element if any
    ("p_sequ", ctypes.POINTER(Sequence)),  # pointer to sequence if any
    ("p_al_err", ctypes.POINTER(DoubleArray)),  # pointer to alignment error array
    ("p_fd_err", ctypes.POINTER(DoubleArray)),  # pointer to field error array
    ("savebeta", ctypes.POINTER(Command)),  # pointer to savebeta command if any
    ("cl", ctypes.c_void_p),  # pointer to constraint list during match
    ("obs_orbit", ctypes.POINTER(DoubleArray)),  # for track observation point
    ("orbit_ref", ctypes.POINTER(DoubleArray)),  # for threader orbit + cum. matrix
    ("interp_at", ctypes.POINTER(DoubleArray)),  # array of positions for intermediate values
    # RF-Multipole errors (EFCOMP)
    ("p_ph_err", ctypes.POINTER(DoubleArray)),  # pointer to rf-multipole phase error array AL:
    # RF-Multipoles
    ("rfm_volt", ctypes.c_double),  # volt of the main rf-multipole field  AL:
    ("rfm_freq", ctypes.c_double),  # frequency of the rf-multipole fields  AL:
    ("rfm_harmon", ctypes.c_int),  # harmonic number of the rf-multipole fields  AL:
    ("rfm_lag", ctypes.c_double),  # lag of the rf-multipole fields  AL:
]


NodeList._fields_ = [
    ("stamp", ctypes.c_int),
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("name_list", ctypes.POINTER(NameList)),  # index list of node (!) names. node_name = el_name:occ_cnt
    ("nodes", ctypes.POINTER(ctypes.POINTER(Node))),  # node pointer list
]


Command._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("module", ctypes.c_char * _NAME_L),  # name of module it belongs to
    ("group", ctypes.c_char * _NAME_L),  # command group it belongs to
    ("stamp", ctypes.c_int),
    ("link_type", ctypes.c_int),  # 0 none, 1 start, 2 end of group
    ("mad8_type", ctypes.c_int),  # 0 none, else mad-8 element code
    ("beam_def", ctypes.c_int),  # beam commands: 1 if defined
    ("par_names", ctypes.POINTER(NameList)),  # names + input flag of parameters
    ("par", ctypes.POINTER(CommandParameterList)),  # parameter pointer list
]


CommandParameter._fields_ = [          # holds one command parameter */
    ("name", ctypes.c_char * _NAME_L),
    ("type", ctypes.c_int),  # 0 logical 1 integer 2 double
                             # 3 string 4 constraint */
                             # 11 int array 12 double array
                             # 13 string array */
    ("c_type", ctypes.c_int),  # for type 4:
                               # 1 min, 2 max, 3 both, 4 value */
    ("double_value", ctypes.c_double),  # type 0, 1, 2, 4 */
    ("c_min", ctypes.c_double),  # type 4 */
    ("c_max", ctypes.c_double),  # type 4 */
    ("expr", ctypes.POINTER(Expression)),  # type 1, 2, 4 */
    ("min_expr", ctypes.POINTER(Expression)),  # type 4 */
    ("max_expr", ctypes.POINTER(Expression)),  # type 4 */
    ("string", ctypes.c_char_p),  # type 3 */
    ("stamp", ctypes.c_int),
    ("double_array", ctypes.POINTER(DoubleArray)),   # type 11, 12 */
    ("expr_list", ctypes.POINTER(ExprList)),         # type 11, 12 */
    ("m_string", ctypes.POINTER(CharPArray)),  # type 13 */
    ("call_def", ctypes.POINTER(CommandParameter)),  # contains definitions for "bare"
                                                     # parameter input, e.g. option,echo */
]


CommandParameterList._fields_ = [
    ("stamp", ctypes.c_int),
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("parameters", ctypes.POINTER(ctypes.POINTER(CommandParameter))),  # command_parameter pointer list
]


Expression._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("string", ctypes.c_char_p),  # expression in string form */
    ("status", ctypes.c_int),  # status flag: 0 not evaluated 1 evaluated */
    ("polish", ctypes.POINTER(IntArray)),  # pointer to Polish notation, or NULL */
    ("value", ctypes.c_double),  # actual value */
    ("stamp", ctypes.c_int),
]


ExprList._fields_ = [
    ("stamp", ctypes.c_int),
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation
    ("list", ctypes.POINTER(ctypes.POINTER(Expression)))  # expression pointer list
]


InBuff._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("flag", ctypes.c_int),
    ("c_a", ctypes.POINTER(CharArray)),
    ("stamp", ctypes.c_int),
]


InBuffList._fields_ = [
    ("name", ctypes.c_char * _NAME_L),
    ("max", ctypes.c_int),  # max. pointer array size
    ("curr", ctypes.c_int),  # current occupation = call level
    ("input_files", ctypes.POINTER(ctypes.c_void_p)),  # input file pointers
    ("stamp", ctypes.c_int),
    ("buffers", ctypes.POINTER(ctypes.POINTER(InBuff)))  # in_buff pointer list
]
