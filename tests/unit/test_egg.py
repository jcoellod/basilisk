import sys
import pytest
import context
import egg


def test_import_in_proccess():
    returned = egg.spawn_basilisk(_function_with_bsk, "Success")
    assert not [mod for mod in sys.modules if "basilisk" in mod]
    assert returned == "Success"


def test_egg_nest():
    arguments = [("Success{}".format(i),) for i in range(10)]
    returned = egg.spawn_nest(
        _function_with_bsk,
        arguments,
    )
    assert not [mod for mod in sys.modules if "basilisk" in mod]
    assert all([returned[i] == arguments[i][0] for i in range(10)])


def test_spawn_this_decorator():
    wrapped = egg.spawn_this(_function_with_bsk)
    returned = wrapped("Success")
    assert not [mod for mod in sys.modules if "basilisk" in mod]
    assert returned == "Success"


def _function_with_bsk(bsk, some_text):
    with pytest.raises(IOError):
        bsk.call("Not a file")
    assert [mod for mod in sys.modules if "basilisk" in mod]
    return some_text
