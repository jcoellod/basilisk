from lark import Lark


madx_parser = Lark(r"""
    program: statement*

    statement: title
             | if_statement
             | while_statement
             | definition
             | assignment
             | command
             | macro_decl
             | macro_exec
   
    title: "title"i ESCAPED_STRING ";"
    command: VARNAME ("," attribute)* ";"
    definition: VARNAME ":" (command | line)
    line: "LINE"i "=" "(" line_elem ("," line_elem)* ")" ";"
    line_elem: VARNAME
             | "-" line_elem
             | "(" line_elem ")"
             | INT "*" line_elem
             | line

    if_statement: "IF"i cond_and_body ("ELSEIF"i cond_and_body)* ("ELSE"i  "{" program "}")? ";"?
    while_statement: "WHILE"i cond_and_body ";"?

    macro_decl: VARNAME macro_vars? ":" "MACRO"i "=" "{" program "}" ";"?
    macro_vars: "(" VARNAME ("," VARNAME)* ")"
    macro_exec: "exec"i "," VARNAME ("(" (expression ("," expression)*)? ")")? ";"

    cond_and_body: "(" logical_expression ")" "{" program "}"

    attribute: expression | VARNAME ["="|":="] attribute_value
    attribute_value: WORD
                   | ESCAPED_STRING
                   | expression
                   | "{" expression ("," expression)* "}"

    assignment: modifier? VARNAME ["="|":="] expression ";"
    modifier: "REAL"i | "INT"i | "CONST"i | "CONST"i "REAL"i | "CONST"i "INT"i
            | "REAL"i "CONST"i | "INT"i "CONST"i

    expression: SIGNED_FLOAT
              | SIGNED_INT
              | VARNAME
              | VARNAME ("->" VARNAME)+
              | funccall
              | expression bin_operator expression
              | "(" expression ")"
              | "-" expression


    logical_expression: expression comp_operator expression
                      | "(" logical_expression ")"
                      | logical_expression "||" logical_expression
                      | logical_expression "&&" logical_expression

    funccall: funcname "(" expression ")"
    funcname: "SQRT"i | "LOG"i | "LOG10"i | "EXP"i | "SIN"i | "COS"i | "TAN"i
            | "ASIN"i | "ACOS"i | "ATAN"i | "SINH"i | "COSH"i | "TANH"i | "SINC"i
            | "ABS"i | "ERF"i | "ERFC"i | "FLOOR"i | "CEIL"i | "ROUND"i | "FRAC"i
            | "RANF"i | "GAUSS"i | "TGAUSS"i 

    comp_operator: ">=" | "<=" | "==" | "<>" | ">" | "<"
    bin_operator: "+" | "-" | "*" | "/" | "^"

    BLOCK_COMMENT: /\/\*(\*(?!\/)|[^*])*\*\//
    LINE_COMMENT: "//" /[^\n]/*
                | "!" /[^\n]/*

    VARNAME: LETTER ("_"|"."|LETTER|DIGIT)*

    %import common.ESCAPED_STRING
    %import common.WORD
    %import common.LETTER
    %import common.DIGIT
    %import common.SIGNED_FLOAT
    %import common.INT
    %import common.SIGNED_INT
    %import common.WS
    %ignore LINE_COMMENT
    %ignore BLOCK_COMMENT
    %ignore WS
""", start="program", parser="lalr")
