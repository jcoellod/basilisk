from __future__ import division
from os.path import join, abspath, dirname
from operator import add, sub, mul, truediv
import pytest
import pandas as pd
import context
import basilisk as bsk
from expressions.table import Table


def test_bsk_float_compatibility():
    testvar1 = bsk.static(5)
    assert float(7. + testvar1) == 7. + 5.
    assert float(testvar1 + 7.) == 5. + 7.
    assert float(testvar1 / 2.) == 5. / 2.
    assert float(2. / testvar1) == 2. / 5.


def test_bsk_simple_operations():
    testvar1 = bsk.static(5)
    testvar2 = bsk.static(10)
    for operation in (add, sub, mul, truediv, pow):
        testvar3 = bsk.dynamic(operation(testvar1, testvar2))
        assert isinstance(testvar3, bsk.Variable)
        assert float(testvar3) == operation(5., 10.)


def test_bsk_complex_operations():
    testvar1 = bsk.static(5)
    testvar2 = bsk.static(10)
    testvar3 = (testvar1 + testvar2) * testvar2 / testvar1
    assert float(testvar3) == 30.


def test_control_raises_if_doesnt_exists():
    with pytest.raises(ValueError):
        bsk.control("testvar")


def test_dynamic_variable():
    var1 = bsk.static(100.)
    dynvar = bsk.dynamic(var1 + 5)
    assert float(dynvar) == 105.
    var1.set(200.)
    assert float(dynvar) == 205.


def test_sequence_item_element():
    pass


def test_table_as_dataframe(_alba_lattice_path):
    bsk.call(_alba_lattice_path)
    bsk.raw("""
    beam, sequence=machine, particle=electron, energy=3;
    use, period=machine;
    twiss;
    """)
    twiss_table = bsk.table("twiss")
    assert isinstance(twiss_table, Table)
    twiss_df, headers = twiss_table.as_dataframe()
    assert isinstance(twiss_df, pd.DataFrame)
    assert isinstance(headers, dict)
    assert headers["NAME"] == "twiss"
    assert "BETX" in twiss_df.columns


def test_bsk_set_and_get_table():
    testdf = pd.DataFrame({"NAME": ["test1", "test2", "test3"],
                           "TESTDATA": [2.34, 0.123, 7]})
    resdf, _ = Table.from_dataframe(
        "testtable", "testtable", testdf
    ).as_dataframe()
    for col in testdf.columns:
        assert all(testdf.loc[:, col] == resdf.loc[:, col])


def test_madx_as_docstring():
    _docstring_madx(value=7)


@bsk.madx
def _docstring_madx(value=None):
    """
    a = {value};
    """
    assert float(bsk.control("a")) == 7


@pytest.fixture
def _alba_lattice_path():
    return abspath(join(dirname(__file__), "..",
                        "sequences", "ALBA-25.4th_v2.seq"))
